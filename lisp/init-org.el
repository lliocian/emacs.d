;;                                            _      
;;   ___  _ __ __ _       _ __ ___   ___   __| | ___ 
;;  / _ \| '__/ _` |_____| '_ ` _ \ / _ \ / _` |/ _ \
;; | (_) | | | (_| |_____| | | | | | (_) | (_| |  __/
;;  \___/|_|  \__, |     |_| |_| |_|\___/ \__,_|\___|
;;            |___/                                  

;; highlight in org-mode
(require 'org)
(setq org-src-fontify-natively t)

;; agenda
;; agenda folder
(setq org-agenda-files '("~/org"))
;; agenda key
(global-set-key (kbd "C-c a") 'org-agenda)

;; end of init-org.el
(provide 'init-org)
