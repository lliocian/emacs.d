;;      _                  ____            _       _   
;;     | | __ ___   ____ _/ ___|  ___ _ __(_)_ __ | |_ 
;;  _  | |/ _` \ \ / / _` \___ \ / __| '__| | '_ \| __|
;; | |_| | (_| |\ V / (_| |___) | (__| |  | | |_) | |_ 
;;  \___/ \__,_| \_/ \__,_|____/ \___|_|  |_| .__/ \__|
;;                                          |_|        

(defun my-web-mode-indent-setup ()
  (setq web-mode-markup-indent-offset 2) ; web-mode, html tag in html file
  (setq web-mode-css-indent-offset 2)    ; web-mode, css in html file
  (setq web-mode-code-indent-offset 2)   ; web-mode, js code in html file
  )
(add-hook 'web-mode-hook 'my-web-mode-indent-setup)

(add-hook 'js2-mode-hook (lambda () (setq js2-basic-offset 2)))	; set indent to 2 spaces
;; end of init-js.el
(provide 'init-js)
