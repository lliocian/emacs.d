;;      _       __             _ _       
;;   __| | ___ / _| __ _ _   _| | |_ ___ 
;;  / _` |/ _ \ |_ / _` | | | | | __/ __|
;; | (_| |  __/  _| (_| | |_| | | |_\__ \
;;  \__,_|\___|_|  \__,_|\__,_|_|\__|___/

(setq inhibit-splash-screen 1)  ;; close welcome
(setq make-backup-files nil)  ;; close auto backup file
(setq auto-save-default nil)  ;; close auto save file
(setq ring-bell-function 'ignore)  ;; close error ring by default
(setq-default cursor-type 'bar)  ;; set cursor by default
(setq auto-mode-alist
      (append
       '(("\\.html\\'" . web-mode))
       '(("\\.js\\'" . js2-mode))
       '(("\\.jsx\\'" . js2-jsx-mode))
       auto-mode-alist))
(require 'emmet-mode)
(add-hook 'web-mode-hook 'emmet-mode)  ;; emmet-mode for web-mode

(global-hl-line-mode 1)  ;; highlight current line
(global-linum-mode 1)  ;; show line number
(global-auto-revert-mode 1)  ;; auto reload modified file
(global-company-mode 1)  ;; company

;(electric-indent-mode -1)  ;; indent
(delete-selection-mode 1)  ;; delete
(tool-bar-mode -1)  ;; tool bar
(scroll-bar-mode -1)  ;; scroll bar
(fset 'yes-or-no-p 'y-or-n-p)  ;; fast confirm
;(require 'popwin)  ;; popwin plugin
;(popwin-mode 1)

(add-hook 'emacs-lisp-mode-hook 'show-paren-mode)  ;; Highlight Matching Parenthesis

;; better code indentation
(defun indent-buffer()
  (interactive)
  (indent-region (point-min) (point-max)))
(defun indent-region-or-buffer()
  (interactive)
  (save-excursion
    (if (region-active-p)
      (progn
	  (indent-region (region-beginning) (region-end))
	  (message "Indent selected region."))
      (progn
 	(indent-buffer)
	(message "Indent buffer.")))))
(global-set-key (kbd "C-M-\\") 'indent-region-buffer)

;; Abbreviation for completion
(setq-default abbrev-mode t)
(define-abbrev-table 'global-abbrev-table '(
					    ;; Shifu
					    ("8zl" "zilongshanren")
					    ;; Tudi
					    ("8lxy" "lixinyang")
					    ))

;; Dired mode
(setq dired-recursive-deletess 'always)
(setq dired-recursive-copies 'always)
(put 'dired-find-alternate-file 'disabled nil)
; (require 'dired)  ;; load
; (define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file)
;; lazy load
(with-eval-after-load 'dired
  (define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file))
(require 'dired-x)  ;; "C-x C-j" to current path
(setq dired-dwin-target 1)  ;; when two frames exist, set another window to targeted address of copy

;; show-paren-mode
(define-advice show-paren-function (:around (fn) fix-show-paren-function)
  "Highlight enclosing parens."
  (cond ((looking-at-p "\\s(") (funcall fn))
	(t (save-excursion
	     (ignore-errors (backward-up-list))
	     (funcall fn)))))

;; \r(^M)
(defun hidden-dos-eol ()
  "Do not show ^M in files containing mixed UNIX and DOS line endings."
  (interactive)
  (unless buffer-display-table
    (setq buffer-display-table (make-display-table)))
  (aset buffer-display-table ?\^M []))

;; end of init-defaults.el
(provide 'init-defaults)
