;;  _              _     _           _ _                 
;; | | _____ _   _| |__ (_)_ __   __| (_)_ __   __ _ ___ 
;; | |/ / _ \ | | | '_ \| | '_ \ / _` | | '_ \ / _` / __|
;; |   <  __/ |_| | |_) | | | | | (_| | | | | | (_| \__ \
;; |_|\_\___|\__, |_.__/|_|_| |_|\__,_|_|_| |_|\__, |___/
;;           |___/                             |___/     


;; fast open config file function
(defun open-init-file()
  (interactive)
  (find-file "~/.emacs.d/init.el"))

;; bind open config file function to <f2>
(global-set-key (kbd "<f2>") 'open-init-file)

;; open recent file
(require 'recentf)
(recentf-mode 1)
(setq rectntf-max-menu-item 10)
(global-set-key (kbd "C-x C-r") 'recentf-open-files)

;; end of init-keybindings.el
(provide 'init-keybindings)
