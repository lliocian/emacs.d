;;                        _             
;;  _   _  ___  _   _  __| | __ _  ___  
;; | | | |/ _ \| | | |/ _` |/ _` |/ _ \ 
;; | |_| | (_) | |_| | (_| | (_| | (_) |
;;  \__, |\___/ \__,_|\__,_|\__,_|\___/ 
;;  |___/                               
;;                        _             
;;  _   _  ___  _   _  __| | __ _  ___  
;; | | | |/ _ \| | | |/ _` |/ _` |/ _ \ 
;; | |_| | (_) | |_| | (_| | (_| | (_) |
;;  \__, |\___/ \__,_|\__,_|\__,_|\___/ 
;;  |___/                               

(require 'youdao-dictionary)
(setq url-automatic-caching t)
(global-set-key (kbd "C-c y") 'youdao-dictionary-search-at-point)
(provide 'init-youdao-dictionary)
