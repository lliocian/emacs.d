;;                   _                         
;;  _ __   __ _  ___| | ____ _  __ _  ___  ___ 
;; | '_ \ / _` |/ __| |/ / _` |/ _` |/ _ \/ __|
;; | |_) | (_| | (__|   < (_| | (_| |  __/\__ \
;; | .__/ \__,_|\___|_|\_\__,_|\__, |\___||___/
;; |_|                         |___/           
;;                                                              _   
;;  _ __ ___   __ _ _ __   __ _  __ _  ___ _ __ ___   ___ _ __ | |_ 
;; | '_ ` _ \ / _` | '_ \ / _` |/ _` |/ _ \ '_ ` _ \ / _ \ '_ \| __|
;; | | | | | | (_| | | | | (_| | (_| |  __/ | | | | |  __/ | | | |_ 
;; |_| |_| |_|\__,_|_| |_|\__,_|\__, |\___|_| |_| |_|\___|_| |_|\__|
;;                              |___/                               

(when (>= emacs-major-version 24)
    (require 'package)
    (package-initialize)
    (setq package-archives '(("gnu"   . "http://elpa.emacs-china.org/gnu/")
			 ("melpa" . "http://elpa.emacs-china.org/melpa/"))))

;; cl - Common Lisp Extension
(require 'cl)

;; Add Package
(defvar my/packages '(
                           ;; --- Auto-completion ---
			   company
			   ;; --- Better Editor ---
			   ;smooth-scrolling
			   hungry-delete
			   swiper
			   counsel
			   smartparens
			   ;; --- Major Mode ---
			   js2-mode
			   js2-refactor
			   web-mode
			   markdown-mode
			   ;; --- Minor Mode ---
			   emmet-mode
			   ;; Quick Note Taking
			   ;deft
			   ;; JavaScript REPL
			   nodejs-repl
			   ;exec-path-from-shell ;; OS X only
			   ;; --- Themes ---
			   monokai-theme
			   solarized-theme
			   doom-themes
			   ;; Chinese input method
			   pyim
			   ;; translate
			   youdao-dictionary
			   ;; ......
			   ) "Default packages")

(setq package-selected-packages my/packages)

(defun my/packages-installed-p ()
  (loop for pkg in my/packages
	when (not (package-installed-p pkg)) do (return nil)
	finally (return t)))

(unless (my/packages-installed-p)
  (message "%s" "Refreshing package database...")
  (package-refresh-contents)
  (dolist (pkg my/packages)
    (when (not (package-installed-p pkg))
      (package-install pkg))))

;; Find Executable Path on OS X
;(when (memq window-system '(max ns))
;  (exec-path-from-shell-initialize))

;; end of init-packages.el
(provide 'init-packages)
