(package-initialize)

(add-to-list 'load-path "~/.emacs.d/lisp/")

;; Package Management
;; ----------------------------------------
(require 'init-packages)
(require 'init-defaults)
(require 'init-keybindings)
(require 'init-org)
(require 'init-ui)
(require 'init-pyim)
(require 'init-youdao-dictionary)
(require 'init-js)
;; manage emacs's config by org-mode
; (package-initialize)
; (require 'org-install)
; (require 'ob-tangle)
; (org-babel-load-file (expand-file-name "org-file-name.org" user-emacs-directory))
